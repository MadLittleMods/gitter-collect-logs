# Collect Gitter logs locally

Quick and dirty way to download logs from the Gitter servers

Tested on Windows 10 with Cmder (maybe cygwin).

Adjust the values in `gitter-collect-logs.js` then run the following command,

```sh
$ npm start
```

The script will run each command serially


## Just use Ansible instead

Instead of trying to pull the logs locally and search, you can also grep directly on each machine with Ansible

```sh
$ cd /opt/gitter-infrastructure/ansible/
$ ansible webapp-* -i prod -m shell -a 'grep ...' --become
```

See the Gitter infrastructure project for more details, https://gitlab.com/gitlab-com/gl-infra/gitter-infrastructure/


## Search/grep the logs

```sh
$ grep -Rnwi "Error during GitHub OAUTH process" "C:\Users\MLM\Downloads\gitter-logs"
```



## Known errors

Disable SSH host key verification or just SSH into all the hosts your interested in to add them to your `known_hosts`

```sh
stderr: 'Pseudo-terminal will not be allocated because stdin is not a terminal.\r\nHost key verification failed.\r\n'
```