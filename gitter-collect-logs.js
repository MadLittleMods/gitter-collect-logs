const path = require("path");
const assert = require("assert");
const fs = require("fs-extra");
const ChildExecutor = require("./lib/child-executor");

// ssh -t webapp-01.prod.gitter "sudo mkdir -p /home/deployer/delete-me-logs/ && sudo cp /var/log/upstart/gitter-web-1.log.1 /home/deployer/delete-me-logs/gitter-web-1.log.1 && sudo chown deployer /home/deployer/delete-me-logs/gitter-web-1.log.1" && (mkdir -p C:\Users\MLM\Downloads\gitter-logs\webpapp-01 || true) && scp deployer@webapp-01.prod.gitter:/home/deployer/delete-me-logs/gitter-web-1.log.1 ~/Downloads/gitter-logs/webpapp-01  && ssh -t webapp-01.prod.gitter "sudo rm -rf /home/deployer/delete-me-logs/"
// ssh -t webapp-01.prod.gitter "sudo mkdir -p /home/deployer/delete-me-logs/ && sudo cp /var/log/upstart/gitter-web-1.log.1 /home/deployer/delete-me-logs/gitter-web-1.log.1 && sudo cp /var/log/upstart/gitter-web-2.log.1 /home/deployer/delete-me-logs/gitter-web-2.log.1 && sudo chown deployer /home/deployer/delete-me-logs/gitter-web-1.log.1 /home/deployer/delete-me-logs/gitter-web-2.log.1" && (mkdir -p C:\Users\MLM\Downloads\gitter-logs\webpapp-01 || true) && scp deployer@webapp-01.prod.gitter:'/home/deployer/delete-me-logs/gitter-web-1.log.1 /home/deployer/delete-me-logs/gitter-web-2.log.1' ~/Downloads/gitter-logs/webpapp-01 && ssh -t webapp-01.prod.gitter "sudo rm -rf /home/deployer/delete-me-logs/"

// Search logs
// grep -Rnwi "Error during GitHub OAUTH process" "C:\Users\MLM\Downloads\gitter-logs"

const HOSTS = [
  /* */
  "webapp-01.prod.gitter",
  "webapp-02.prod.gitter",
  "webapp-03.prod.gitter",
  "webapp-04.prod.gitter",
  "webapp-05.prod.gitter",
  "webapp-06.prod.gitter",
  "webapp-07.prod.gitter",
  "webapp-08.prod.gitter"
  /* */
  /* * /
	'mongo-replica-01.prod.gitter',
	'mongo-replica-02.prod.gitter',
	'mongo-replica-03.prod.gitter',
	/* */
  /* * /
	'ws-01.prod.gitter',
	'ws-02.prod.gitter',
	'ws-03.prod.gitter',
	'ws-04.prod.gitter',
	'ws-05.prod.gitter',
	'ws-06.prod.gitter',
	'ws-07.prod.gitter',
	'ws-08.prod.gitter',
	/* */
];

const FILES = [
  /* */
  "/var/log/upstart/gitter-api-1.log",
  "/var/log/upstart/gitter-api-1.log.1",
  "/var/log/upstart/gitter-api-2.log.1",
  "/var/log/upstart/gitter-web-1.log.1",
  "/var/log/upstart/gitter-web-2.log.1"
  /* */
  //'/var/log/mongodb/mongod.log',
  //'/var/log/mongodb/mongod.log-20190326.gz',
  /* * /
	'/var/log/upstart/gitter-websockets-1.log.1',
	'/var/log/upstart/gitter-websockets-1.log.2.gz',
	'/var/log/upstart/gitter-websockets-1.log.3.gz',
	'/var/log/upstart/gitter-websockets-1.log.4.gz',
	'/var/log/upstart/gitter-websockets-1.log.5.gz',
	'/var/log/upstart/gitter-websockets-1.log.6.gz',
	'/var/log/upstart/gitter-websockets-1.log.7.gz',
	'/var/log/upstart/gitter-websockets-2.log.1',
	'/var/log/upstart/gitter-websockets-2.log.2.gz',
	'/var/log/upstart/gitter-websockets-2.log.3.gz',
	'/var/log/upstart/gitter-websockets-2.log.4.gz',
	'/var/log/upstart/gitter-websockets-2.log.5.gz',
	'/var/log/upstart/gitter-websockets-2.log.6.gz',
	'/var/log/upstart/gitter-websockets-2.log.7.gz',
	/* */
];

const LOCAL_FILE_DESTINATION = "C:\\Users\\MLM\\Downloads\\gitter-logs\\";

async function generateRemotePrepCommandForHost(host, files) {
  assert(files);
  assert(files.length > 0);

  const copyFilesToDeployerFriendlyDirectoryCommand = files
    .map(file => {
      const fileName = path.basename(file);
      return ` && (sudo cp ${file} /home/deployer/delete-me-logs/${fileName} || true) && (sudo chown deployer /home/deployer/delete-me-logs/${fileName} || true)`;
    })
    .join("");

  const prepareRemoteFilesCommand = `ssh -oStrictHostKeyChecking=no -t ${host} "sudo mkdir -p /home/deployer/delete-me-logs/ ${copyFilesToDeployerFriendlyDirectoryCommand}`;

  return prepareRemoteFilesCommand;
}

async function generateDownloadFilesCommandForHost(host, files) {
  assert(files);
  assert(files.length > 0);

  const resultantLocalDestination = path.join(LOCAL_FILE_DESTINATION, host);

  await fs.mkdirp(resultantLocalDestination);

  const scpFilesCommand = files
    .map(file => {
      const fileName = path.basename(file);
      return ` && (scp deployer@${host}:/home/deployer/delete-me-logs/${fileName} ${resultantLocalDestination} || true)`;
    })
    .join("");

  const downloadFilesCommand = `echo 'Downloading...' ${scpFilesCommand}`;

  return downloadFilesCommand;
}

async function generateCleanupCommandForHost(host, files) {
  const cleanupCommand = `ssh -t ${host} "sudo rm -rf /home/deployer/delete-me-logs/"`;
  return cleanupCommand;
}

async function runCommandsForHost(host, files) {
  const commandsToRun = [
    await generateRemotePrepCommandForHost(host, files),
    await generateDownloadFilesCommandForHost(host, files),
    await generateCleanupCommandForHost(host, files)
  ];

  for (let command of commandsToRun) {
    const ChildExecutor = require("./lib/child-executor");
    const executor = new ChildExecutor();

    console.log(`\tCommand:`, command);
    try {
      await executor.exec(command);
    } catch (err) {
      console.log("Error running command: ", err);
    }
  }
}

(async () => {
  try {
    for (let host of HOSTS) {
      console.log(`Starting host ${host} -------------------------`);
      await runCommandsForHost(host, FILES);
    }
  } catch (err) {
    console.log("err", err);
    throw err;
  }
})();
